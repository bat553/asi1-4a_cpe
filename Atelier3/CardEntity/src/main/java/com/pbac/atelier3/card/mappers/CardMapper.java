package com.pbac.atelier3.card.mappers;

import com.pbac.atelier3.card.models.Card;
import com.pbac.atelier3.common_libs.dto.card.CardsDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CardMapper {
    CardMapper INSTANCE = Mappers.getMapper(CardMapper.class);
    Card toModel(CardsDto cardDto);
}
