package com.pbac.atelier3.card.controllers;

import com.pbac.atelier3.card.mappers.CardMapper;
import com.pbac.atelier3.card.models.Card;
import com.pbac.atelier3.card.services.CardsService;
import com.pbac.atelier3.card.services.UsersService;
import com.pbac.atelier3.common_libs.auth.AuthentificationService;
import com.pbac.atelier3.common_libs.dto.card.BuyCardDto;
import com.pbac.atelier3.common_libs.dto.card.CardsDto;
import com.pbac.atelier3.common_libs.dto.card.ChangeCardDto;
import com.pbac.atelier3.common_libs.dto.transaction.TransactionsDto;
import com.pbac.atelier3.common_libs.dto.user.UsersDto;
import com.pbac.atelier3.common_libs.exceptions.TransactionError;
import com.pbac.atelier3.common_libs.exceptions.UserError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


@RestController
public class CardsController {
	@Autowired
	CardsService cService;

	@Autowired
	AuthentificationService authentificationService;

	@Autowired
	UsersService uService;
	
	@RequestMapping(value="/api/cards", method = RequestMethod.POST)
	public Card addCard(@RequestBody CardsDto cardDto) {
		Card card = CardMapper.INSTANCE.toModel(cardDto);
		cService.addCard(card);
		return card;
	}
	
	@RequestMapping(value="/api/cards/{id}", method=RequestMethod.GET)
	public Card getCard(@PathVariable String id) {
		return cService.getCard(id);
	}

	@RequestMapping(method= RequestMethod.GET,value="/api/cards")
	public Iterable<Card> getCards() {
		return cService.getCards();
	}

	@RequestMapping(method= RequestMethod.GET,value="/api/cardsUser/{id}")
	public Iterable<Card> getCardsUser(@PathVariable String id) {
		return cService.getCardsUser(id);
	}

	@RequestMapping(method= RequestMethod.GET,value="/api/cardsOnSale")
	public Iterable<Card> getCardsOnSale() {
		return cService.getCardsOnSale();
	}

	@RequestMapping(method= RequestMethod.POST,value="/api/cards/buy")
	public TransactionsDto buyCard(@RequestBody BuyCardDto cardForm, @CookieValue(value = "JWT") String jwt, HttpServletResponse response) {
		String username = authentificationService.getUsername(jwt);
		UsersDto user = uService.findByUsername(username);

		if (user == null){
			throw new UserError("unknown");
		}

		TransactionsDto transaction = cService.buyCard(cardForm.getCardId(), user);
		if (transaction == null){
			throw new TransactionError("error");
		}
		return transaction;
	}



	@RequestMapping(method = RequestMethod.PUT, value = "/api/cards")
	public Card setCardSellStatus(@CookieValue(value = "JWT") String jwt, @RequestBody ChangeCardDto changeCardDto){
		String username = authentificationService.getUsername(jwt);
		UsersDto user = uService.findByUsername(username);

		if (user == null){
			throw new UserError("unknown");
		}

		return cService.setCardSellStatus(changeCardDto, user);
	}
}
