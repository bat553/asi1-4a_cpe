package com.pbac.atelier3.card.services;

import com.pbac.atelier3.card.models.Card;
import com.pbac.atelier3.card.repositories.CardsRepository;
import com.pbac.atelier3.common_libs.dto.card.ChangeCardDto;
import com.pbac.atelier3.common_libs.dto.transaction.TransactionsDto;
import com.pbac.atelier3.common_libs.dto.user.UsersDto;
import com.pbac.atelier3.common_libs.exceptions.CardError;
import com.pbac.atelier3.common_libs.exceptions.TransactionError;
import com.pbac.atelier3.common_libs.exceptions.UserError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CardsService {
	@Autowired
	CardsRepository cRepository;

	@Autowired
	TransactionsService tService;

	@Autowired
	UsersService uService;

	public void addCard(Card c) {
		c.setId(UUID.randomUUID().toString());
		Card createdCard=cRepository.save(c);
		System.out.println(createdCard);
	}

	public TransactionsDto buyCard(String cardId, UsersDto buyer) {
		Card card;
		Optional<Card> tmpCard = cRepository.findById(cardId);
		if (tmpCard.isPresent()) {
			card = tmpCard.get();

			if(buyer != null){
				if(!card.isOnSale()){
					throw new CardError("no_on_sale");
				}

				if(buyer.getBalance() == null || buyer.getBalance() < card.getPrice()){
					throw new TransactionError("no_funds");
				}

				UsersDto seller = uService.findByUsername(card.getUserId());
				if (seller == null){
					throw new UserError("unknown");
				}

				//création d'une transaction
				TransactionsDto transaction = tService.addTransaction(new TransactionsDto(buyer, seller, card.getId(), card.getPrice()));

				//Update de la card
				card.setOnSale(false);
				card.setUserId(buyer.getId());

				// Update users
				uService.updateBalance(seller, seller.getBalance() + card.getPrice());
				uService.updateBalance(buyer, buyer.getBalance() - card.getPrice());

				cRepository.save(card);
				return transaction;
			}
		}
		return null;
	}

	public Card getCard(String id) {
		Optional<Card> cOpt = cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}

	public Iterable<Card> getCards() {
		return cRepository.findAll();
	}

	public List<Card> getCardsUser(String id) {
		Iterable<Card> tmpCards = cRepository.findAll();
		List<Card> cards = new ArrayList<>();
		System.out.println(id);

		for (Card card:tmpCards) {
			if(card.getUserId().compareTo(id) == 0){
				cards.add(card);
			}
		}
		return cards;
	}

	public List<Card> getCardsOnSale() {
		Iterable<Card> tmpCards = cRepository.findAll();
		List<Card> cards = new ArrayList<>();
		for (Card card:tmpCards) {
			if(card.isOnSale()){
				cards.add(card);
			}
		}
		return cards;
	}

	public Card setCardSellStatus(ChangeCardDto cardForm, UsersDto user) {
		Card card = this.getCard(cardForm.getCardId());
		if (card == null){
			throw new CardError("not_found");
		}
		if (card.getPrice() == null || card.getPrice() < 1){
			throw new CardError("null_price");
		}

		if(!card.getUserId().equals(user.getId())){
			throw new CardError("not_owned_by_you");
		}

		card.setOnSale(cardForm.isOnSale());
		card.setPrice(cardForm.getPrice());

		this.cRepository.save(card);

		return card;
	}
}
