package com.pbac.atelier3.card.repositories;

import java.util.List;

import com.pbac.atelier3.card.models.Card;
import org.springframework.data.repository.CrudRepository;

public interface CardsRepository extends CrudRepository<Card, String>{
	
	public List<Card> findByName(String name); 
}
