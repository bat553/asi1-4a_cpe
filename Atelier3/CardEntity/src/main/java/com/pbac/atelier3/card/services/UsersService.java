package com.pbac.atelier3.card.services;

import com.pbac.atelier3.common_libs.dto.user.UpdateBalanceDto;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.pbac.atelier3.common_libs.dto.user.UsersDto;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Optional;

@Service
public class UsersService {
    private final RestTemplate restTemplate;

    private final String UserEndpoint = "http://localhost:8083/api/users";


    public UsersService(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.rootUri(UserEndpoint).build();
    }


    public UsersDto findByUsername(String username){
        return this.restTemplate.getForObject(UserEndpoint + "/" + username, UsersDto.class);
    }

    public UpdateBalanceDto updateBalance(UsersDto user, Double newBalance){
        UpdateBalanceDto updateBalanceDto = new UpdateBalanceDto(user.getUsername(), newBalance);

        return this.restTemplate.postForObject(UserEndpoint + "/balance", updateBalanceDto, UpdateBalanceDto.class);
    }

}
