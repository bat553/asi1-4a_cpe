package com.pbac.atelier3.card.services;
import com.pbac.atelier3.common_libs.dto.transaction.TransactionsDto;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TransactionsService {

    private final RestTemplate restTemplate;

    private final String TransactionsEndpoint = "http://localhost:8082/api/transactions";

    public TransactionsService(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.rootUri(TransactionsEndpoint).build();
    }

    public TransactionsDto addTransaction(TransactionsDto transaction) {
        return this.restTemplate.postForObject(this.TransactionsEndpoint, transaction, TransactionsDto.class);
    }
}
