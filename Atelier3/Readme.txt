AMEGLIO-PELLARIN-BEAUD-CAMBRAY

REALISE :

VIDEO : https://youtu.be/n1MPRNiWTXY

Fonctionnement autonome par service :
	- Auth/User : PELLARIN
	- Transaction : CAMBRAY
	- Cards : BEAUD
	- Room : AMEGLIO

Fonctionnement en mode micro-service non-dockerisé:
	- PELLARIN


Fonctionnement du reverse Proxy :
	- PELLARIN

Adaptation du Frontend : 
	- CAMBRAY
	- PELLARIN

Début de l'architecture Docker (Dockerfile/Docker compose) :
	- PELLARIN
	
Réponses questions Altelier 3:
	- CAMBRAY


NON REALISE :

- Conteneurisation
- Room + Jeu

