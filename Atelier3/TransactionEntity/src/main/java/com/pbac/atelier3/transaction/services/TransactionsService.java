package com.pbac.atelier3.transaction.services;

import com.pbac.atelier3.transaction.models.Transactions;
import com.pbac.atelier3.transaction.repositories.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class TransactionsService {

    @Autowired
    TransactionsRepository transactionsRepository;

    public Transactions addTransaction(Transactions transactionDao) {
        transactionDao.setId(UUID.randomUUID().toString());
        transactionDao.setTransactionDate(new Date());
        return transactionsRepository.save(transactionDao);
    }


    public Transactions getTransaction(String id){
        System.out.println("On essaye de chercher la transaction avec l'identifiant = " + id);
        Optional<Transactions> transactionsDaoOptional = transactionsRepository.findById(id);
        if(transactionsDaoOptional.isPresent()){
            return transactionsDaoOptional.get();
        } else {
            return null;
        }
    }

    public Iterable<Transactions> getTransactions() {
        return transactionsRepository.findAll();
    }
}
