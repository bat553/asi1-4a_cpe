package com.pbac.atelier3.transaction.controllers;

import com.pbac.atelier3.common_libs.exceptions.TransactionError;
import com.pbac.atelier3.transaction.models.Transactions;
import com.pbac.atelier3.transaction.services.TransactionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransactionsController {

    @Autowired
    TransactionsService tService;

    @RequestMapping(method = RequestMethod.POST, value = "/api/transactions")
    public Transactions createTransaction(@RequestBody Transactions transactionsDao){
        Transactions transactions = tService.addTransaction(transactionsDao);
        if (transactions == null) {
            throw new TransactionError("creation");
        }
        return transactions;
    }

    @RequestMapping(method= RequestMethod.GET,value="/api/transactions")
    public Iterable<Transactions> getTransactions() {
        return tService.getTransactions();
    }

    @RequestMapping(method=RequestMethod.GET,value="/api/transactions/{id}")
    public Transactions getTransaction(@PathVariable String id) {
        return tService.getTransaction(String.valueOf(id));
    }

}
