package com.pbac.atelier3.transaction.models;



import com.pbac.atelier3.common_libs.exceptions.TransactionError;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="transaction")
public class Transactions {
    @Id
    private String id;
    private Date transactionDate;

    private String buyer_id;

    private String seller_id;

    private String card_id;
    private Double prix;

    public Transactions() {}

    public Transactions(String buyer_id, String seller_id, String card_id, Double prix) {
        super();
        this.id = UUID.randomUUID().toString();
        this.transactionDate = new Date();
        if(Objects.equals(buyer_id, seller_id)){
            throw new TransactionError("same_seller_buyer");
        }
        this.buyer_id = buyer_id;
        this.seller_id = seller_id;
        this.card_id = card_id;
        this.prix = prix;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
    public Date getTransactionDate() {
        return this.transactionDate;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Double getPrix() {
        return prix;
    }

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }
}

