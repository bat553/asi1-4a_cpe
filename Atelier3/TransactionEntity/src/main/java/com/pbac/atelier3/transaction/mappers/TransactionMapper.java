package com.pbac.atelier3.transaction.mappers;

import com.pbac.atelier3.common_libs.dto.transaction.TransactionsDto;
import com.pbac.atelier3.transaction.models.Transactions;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TransactionMapper {
    TransactionMapper INSTANCE = Mappers.getMapper(TransactionMapper.class);
    @Mapping(target="transactionDate", dateFormat="dd/MM/yyyy")
    Transactions toModel(TransactionsDto transactionDto);
}
