package com.pbac.atelier3.transaction.repositories;

import com.pbac.atelier3.transaction.models.Transactions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransactionsRepository extends CrudRepository<Transactions, Integer> {

    public Optional<Transactions> findById(String id);

}
