package com.pbac.atelier3.common_libs.dto.card;

public class BuyCardDto {
    private String cardId;

    public BuyCardDto(){}

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}
