package com.pbac.atelier3.common_libs.dto.user;

public class UsersDto {
	
	private String id;
	private String username;
	private Double balance;
	private String password;
	
	public UsersDto() {
	}

	public String getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public Double getBalance() {
		return balance;
	}

	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
