package com.pbac.atelier3.common_libs.dto.card;

public class ChangeCardDto {
    private String cardId;
    private boolean onSale;
    private Double price;

    public ChangeCardDto(){}

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isOnSale() {
        return onSale;
    }

    public void setOnSale(boolean onSale) {
        this.onSale = onSale;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}
