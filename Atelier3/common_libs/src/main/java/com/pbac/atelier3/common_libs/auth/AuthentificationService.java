package com.pbac.atelier3.common_libs.auth;

import javax.crypto.spec.SecretKeySpec;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.DefaultJwtSignatureValidator;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
public class AuthentificationService {

    SignatureAlgorithm sa = SignatureAlgorithm.HS256;
    SecretKeySpec secretKeySpec = new SecretKeySpec(getSecret(), sa.getJcaName());

    private final DefaultJwtSignatureValidator validator = new DefaultJwtSignatureValidator(sa, secretKeySpec);

    public static byte[] getSecret(){
        String JWT_SECRET = "Th0m4sceGrosBG";
        return JWT_SECRET.getBytes(StandardCharsets.UTF_8);
    }

    public boolean checkJwt(String jwt){
        String[] chunk = jwt.split("\\.");
        String tokenWithoutSignature = chunk[0] + "." + chunk[1];
        String signature = chunk[2];

        return validator.isValid(tokenWithoutSignature, signature);
    }

    public String getUsername(String jwt){
        return (String) Jwts.parser()
                .setSigningKey(getSecret())
                .parseClaimsJws(jwt)
                .getBody()
                .getSubject();
    }

}
