package com.pbac.atelier3.common_libs.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class TransactionError extends RuntimeException {
    public TransactionError(String code){
        super(String.format("errors.transaction.%s", code));
    }

}

