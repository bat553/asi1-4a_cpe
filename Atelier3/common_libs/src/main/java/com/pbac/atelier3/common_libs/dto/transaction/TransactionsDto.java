package com.pbac.atelier3.common_libs.dto.transaction;

import com.pbac.atelier3.common_libs.dto.card.CardsDto;
import com.pbac.atelier3.common_libs.dto.user.UsersDto;

import java.util.Date;

public class TransactionsDto {
    private String id;
    private Date transactionDate;
    private String buyer_id;
    private String seller_id;
    private Double prix;
    private String card_id;

    public TransactionsDto() {
    }

    public TransactionsDto(UsersDto buyer, UsersDto seller, String card_id, Double price){
        this.buyer_id = buyer.getId();
        this.seller_id = seller.getId();
        this.card_id = card_id;
        this.prix = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }
}
