package com.pbac.atelier3.common_libs.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class CardError extends RuntimeException {
    public CardError(String code){
        super(String.format("errors.card.%s", code));
    }

}

