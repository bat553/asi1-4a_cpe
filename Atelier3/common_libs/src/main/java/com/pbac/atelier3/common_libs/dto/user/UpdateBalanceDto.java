package com.pbac.atelier3.common_libs.dto.user;

public class UpdateBalanceDto {
    private String username;
    private Double balance;

    public UpdateBalanceDto(){}

    public UpdateBalanceDto(String username, Double balance){
        this.balance = balance;
        this.username = username;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
