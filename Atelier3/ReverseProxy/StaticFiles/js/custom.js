var user = {};
var titles = {};

$('.ui.dropdown').dropdown();

$(document).ready(function () {

	get_user();

	$(".btn-action-menu").click(function (e) {
		let action = $(this).data("action");
		if (action === "buy" || action === "sell") {
			titles = get_titles(action);
			$("#table-name").html(titles.tableName);
			$(".page-title").html(titles.pageTitle);
			$("#page-information").html(titles.pageDescription);
			$("#welcome-menu").hide(200);
			get_cards(action);
		} else {
			alert("ToDo (:");
		}
	});

	$(".back-button").click(function (e) {
		$("#card-table").hide(200);
		$("#display-card").hide(200);
		$(".page-title").html("Welcome !");
		$("#page-information").html("What do you want ?");
		$("#welcome-menu").show(200);
	});

	$("body").on('click', '.action-button', function (e) {
		e.preventDefault();
		e.stopPropagation();
		let action = $(this).data("action");
		let id = $(this).parents("tr").data("id");
		if (action === "sell") {
			let price = prompt("Quel sera le prix de cette carte ?");
			if (price >= 0) {
				$.ajax("/api/cards",
					{
						data: JSON.stringify({
							'cardId': id,
							"price": price,
							"onSale": true
						}),
						contentType: 'application/json',
						dataType: 'json',
						method: 'PUT',
						success: function (data, status, xhr) {   // success callback function
							remove_line(id);
						},
						error: function (jqXhr, textStatus, errorMessaged) { // error callback 
							alert(jqXhr.responseJSON.message);
						}
					}
				);
			}
		} else {
			$.ajax("/api/cards/buy",
				{
					data: JSON.stringify({
						'cardId': id
					}),
					contentType: 'application/json',
					dataType: 'json',
					method: 'POST',
					success: function (data, status, xhr) {   // success callback function
						get_cards();
						if (data.buyer) {
							$("#money-user").html(data.buyer.balance);
						}
					},
					error: function (jqXhr, textStatus, errorMessaged) { // error callback 
						alert(jqXhr.responseJSON.message);
					}
				}
			);
		}
	});

	$("tbody").on('click', 'tr', function (e) {
		e.preventDefault();
		let id = $(this).data("id");
		$("#display-card").hide(100);
		get_card_data(id);
	});


});

function get_user() {
	$.ajax("/api/users/current",
		{
			dataType: 'json',
			success: function (data, status, xhr) {   // success callback function
				user = data;
				$("#money-user").html(user.balance);
				$("#name-user").html(user.username);
			},
			error: function (jqXhr, textStatus, errorMessage) { // error callback
				if (jqXhr.status === 515 || jqXhr.responseJSON.message.startsWith("errors.auth") || jqXhr.responseJSON.message.includes("JWT")) {
					window.location.replace("login.html");
				}
			}
		}
	);
}

function get_titles(action = "buy") {
	let titles = {
		"tableName": 'Buy some cards !',
		"buttonTitle": 'Buy',
		"pageTitle": 'Buy cards',
		"action": 'buy',
		"pageDescription": 'Increase your collection !'
	};
	if (action === "sell") {
		titles = {
			"tableName": 'Sell your card(s)',
			"buttonTitle": 'Sell',
			"pageTitle": 'Sell cards',
			"action": 'sell',
			"pageDescription": 'Increase your money !'
		};
	}
	return titles;
}

function get_cards(action = "buy") {
	let url = "/api/cardsOnSale";
	if (user.id) {
		if (action === "sell") {
			url = "/api/cardsUser/" + user.id;
		}
	}
	$.ajax(url,
		{
			dataType: 'json',
			success: function (data, status, xhr) {   // success callback function
				display_cards(data);
			},
			error: function (jqXhr, textStatus, errorMessage) { // error callback 
				alert(errorMessage);
			}
		}
	);
}

function display_cards(cards) {
	let template = document.querySelector("#row");
	let cardContainer = document.querySelector("#table-cards-content");

	$(cardContainer).children("tr").each(function () {
		$(this).remove();	// clean table
	});

	for (const card of cards) {
		let clone = document.importNode(template.content, true);
		newContent = clone.firstElementChild.innerHTML
			.replace(/{{family_src}}/g, card.family_src)
			.replace(/{{family_name}}/g, card.family_name)
			.replace(/{{name}}/g, card.name)
			.replace(/{{description}}/g, card.description)
			.replace(/{{hp}}/g, card.hp)
			.replace(/{{energy}}/g, card.energy)
			.replace(/{{attack}}/g, card.attack)
			.replace(/{{defense}}/g, card.defence)
			.replace(/{{price}}/g, card.price)
			.replace(/{{user}}/g, card.userId);
		clone.firstElementChild.innerHTML = newContent;
		$(clone.firstElementChild).attr("data-id", card.id);
		cardContainer.appendChild(clone);
	}

	$(".text-action").html(titles.buttonTitle);
	$(".action-button").each(function () {
		$(this).attr("data-action", titles.action);
	});
	$("#card-table").show(200);
}

function get_card_data(id) {
	$.ajax("/api/cards/" + id,
		{
			dataType: 'json',
			success: function (data, status, xhr) {   // success callback function
				set_data_card(data);
			},
			error: function (jqXhr, textStatus, errorMessage) { // error callback 
				alert(errorMessage);
			}
		}
	);
}

function set_data_card(card) {
	$("#familyNameId").html(card.family_name);
	$(".cardHP").html(card.hp + " HP");
	$(".cardEnergy").html(card.energy);
	$("#cardNameImgId").html(card.name);
	$("#cardImgId").attr("src", card.img);
	$("#cardDescriptionId").html(card.description);
	$("#cardAttackId").html(card.attack + " Attack");
	$("#cardDefenceId").html("Defence " + card.defence);
	$("#cardPriceId").html(card.price + "$");
	$("#display-card").show(100);
}

function remove_line(id) {
	$("tr[data-id=" + id + "]").remove();
}

function setCookie(cname, cvalue, exdays) {
	const d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	let expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(name) {
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
		if (begin != 0) return null;
	}
	else {
		begin += 2;
		var end = document.cookie.indexOf(";", begin);
		if (end == -1) {
			end = dc.length;
		}
	}
	return decodeURI(dc.substring(begin + prefix.length, end));
}

