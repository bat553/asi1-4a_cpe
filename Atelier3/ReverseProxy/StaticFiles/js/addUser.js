$(document).ready(function () {

	$("#add-user-form").on('submit', function (e) {
		e.preventDefault();
		let password = $("#password").val();
		if (password === $("#repassword").val()) {
			if (password.length < 5) {
				alert("Le mot de passe est trop court !");
			} else {
				$.ajax("/api/users",
					{
						method: "POST",
						data: JSON.stringify({
							'username': $("#username").val(),
							'password': password
						}),
						contentType: 'application/json',
						dataType: 'json',
						success: function (data, status, xhr) {   // success callback function
							window.location.replace("card.html");
						},
						error: function (jqXhr, textStatus, errorMessage) { // error callback
							alert("Nom d'utilisateur déjà utilisé !");
						}
					}
				);
			}
		} else {
			alert("Les mots de passe ne correspondent pas !");
		}

	});

});