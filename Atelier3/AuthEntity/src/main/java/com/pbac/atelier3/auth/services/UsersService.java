package com.pbac.atelier3.auth.services;

import com.pbac.atelier3.common_libs.dto.user.LoginDto;
import com.pbac.atelier3.common_libs.dto.user.UsersDto;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Optional;

@Service
public class UsersService {

    private final RestTemplate restTemplate;

    private final String UserEndpoint = "http://localhost:8083/api/users";


    public UsersService(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.rootUri(UserEndpoint).build();
    }

    public UsersDto checkLogin(LoginDto loginForm){
        Optional<UsersDto> userOptional = Optional.ofNullable(this.findByUsername(loginForm.getUsername())); // TODO : req
        if(userOptional.isPresent() && userOptional.get().getPassword() != null){
            String hashed_password_user = DigestUtils.sha256Hex(loginForm.getPassword());
            if(hashed_password_user.equals(userOptional.get().getPassword())){
                return userOptional.get();
            }
        }
        return null;
    }

    public UsersDto findByUsername(String username){
        return this.restTemplate.getForObject(UserEndpoint + "/" + username, UsersDto.class);
    }

    public Iterable<UsersDto> findAll(){
        return Collections.singleton(new UsersDto());
    }

    public UsersDto updateBalance(UsersDto user, Double newBalance){
        /*user.setBalance(newBalance);
        userRepository.save(user);
        TODO : REQ
        */
        return user;
    }

}
