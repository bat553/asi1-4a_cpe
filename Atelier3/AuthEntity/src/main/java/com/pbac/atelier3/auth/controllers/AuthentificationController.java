package com.pbac.atelier3.auth.controllers;

import com.pbac.atelier3.auth.exceptions.AuthError;
import com.pbac.atelier3.common_libs.dto.user.LoginDto;
import com.pbac.atelier3.common_libs.dto.user.UsersDto;
import com.pbac.atelier3.auth.services.UsersService;
import com.pbac.atelier3.common_libs.auth.AuthentificationService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@RestController
public class AuthentificationController {

    @Autowired
    UsersService usersService;

    @Autowired
    AuthentificationService authentificationService;


    @RequestMapping(method = RequestMethod.POST, value = "/api/auth/login")
    public LoginDto login(@RequestBody LoginDto loginForm, HttpServletResponse response) {
        UsersDto user = usersService.checkLogin(loginForm);
        if (user == null){
            throw new AuthError("bad_login");
        }
        String jwt = Jwts.builder()
                .setIssuer("Aterlier3")
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(6, ChronoUnit.HOURS)))
                .signWith(SignatureAlgorithm.HS256, AuthentificationService.getSecret())
                .setSubject(user.getUsername()).compact();

        Cookie cookie = new Cookie("JWT", jwt);
        cookie.setPath("/");
        response.addCookie(cookie);
        loginForm.setPassword(null);
        return loginForm;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/auth/token")
    public void validate_token(@CookieValue("JWT") String jwt, HttpServletResponse response){
        boolean is_jwt_correct = authentificationService.checkJwt(jwt);

        if(!is_jwt_correct){
            Cookie cookie = new Cookie("JWT", null);
            cookie.setPath("/");
            response.addCookie(cookie);
            throw new AuthError("bad_jwt");
        }
        response.setStatus(HttpStatus.NO_CONTENT.value());

    }

}
