package com.pbac.atelier3.user.mappers;

import com.pbac.atelier3.common_libs.dto.user.UsersDto;
import com.pbac.atelier3.user.models.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	User toModel(UsersDto userDto);
}
