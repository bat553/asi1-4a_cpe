package com.pbac.atelier3.user.repositories;

import com.pbac.atelier3.user.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends CrudRepository<User, String> {
    public Optional<User> findByUsername(String username);

}
