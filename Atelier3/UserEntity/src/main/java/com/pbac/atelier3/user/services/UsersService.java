package com.pbac.atelier3.user.services;

import com.pbac.atelier3.common_libs.dto.user.LoginDto;
import com.pbac.atelier3.common_libs.dto.user.UpdateBalanceDto;
import com.pbac.atelier3.user.models.User;
import com.pbac.atelier3.user.repositories.UsersRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UsersService {

    @Autowired
    UsersRepository userRepository;

    public User addUser(User userDao){
        userDao.setId(UUID.randomUUID().toString());
        return userRepository.save(userDao);
    }

    public User getUser(String id){
        System.out.println("On essaye de chercher l'user avec l'identifiant = " + id);
        Optional<User> usersDaoOptional = userRepository.findById(id);
        if(usersDaoOptional.isPresent()){
            return usersDaoOptional.get();
        } else {
            return null;
        }
    }

    public User checkLogin(LoginDto loginForm){
        Optional<User> userOptional = userRepository.findByUsername(loginForm.getUsername());
        if(userOptional.isPresent() && userOptional.get().getPassword() != null){
            String hashed_password_user = DigestUtils.sha256Hex(loginForm.getPassword());
            if(hashed_password_user.equals(userOptional.get().getPassword())){
                return userOptional.get();
            }
        }
        return null;
    }

    public User findByUsername(String username){
        Optional<User> userOptional = userRepository.findByUsername(username);
        if(userOptional.isPresent()){
            return userOptional.get();
        }
        return null;
    }

    public Iterable<User> findAll(){
        return userRepository.findAll();
    }

    public User updateBalance(User user, UpdateBalanceDto updateBalanceDto){
        user.setBalance(updateBalanceDto.getBalance());
        userRepository.save(user);
        return user;
    }

}
