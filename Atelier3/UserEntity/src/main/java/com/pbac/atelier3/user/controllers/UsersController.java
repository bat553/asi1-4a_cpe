package com.pbac.atelier3.user.controllers;

import com.pbac.atelier3.common_libs.auth.AuthentificationService;
import com.pbac.atelier3.common_libs.dto.user.UpdateBalanceDto;
import com.pbac.atelier3.common_libs.exceptions.UserError;
import com.pbac.atelier3.user.models.User;
import com.pbac.atelier3.user.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {

    @Autowired
    UsersService usersService;

    @Autowired
    AuthentificationService authentificationService;

    @RequestMapping(method = RequestMethod.GET, value = "/api/users/{id}")
    public User getUser(@PathVariable String id){
        User user = usersService.getUser(id);
        if(user == null){
            user = usersService.findByUsername(id);
        }
        return user;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/api/users")
    public User createUser(@RequestBody User userDao){
        User user = usersService.addUser(userDao);
        if (user == null){
            throw new UserError("creation");
        }
        return user;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/users")
    public Iterable<User> getUsers(){
        return usersService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/users/current")
    public User getCurrentUser(@CookieValue(value = "JWT") String jwt){

        String username = authentificationService.getUsername(jwt);

        User user = usersService.findByUsername(username);
        if (user != null){
            return user;
        } else {
            throw new UserError("not_found");
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/users/balance")
    public UpdateBalanceDto updateUserBalance(@RequestBody UpdateBalanceDto updateBalanceDto){

        User user = usersService.findByUsername(updateBalanceDto.getUsername());
        if(user != null){
            usersService.updateBalance(user, updateBalanceDto);
        } else {
            throw new UserError("not_found");
        }

        return updateBalanceDto;
    }

}
