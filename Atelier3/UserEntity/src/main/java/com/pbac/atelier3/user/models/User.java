package com.pbac.atelier3.user.models;

import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {
    @Id
    private String id;
    @Column(unique = true)
    private String username;
    private Double balance;

    private String password;

    public User(){}

    public Double getBalance() {
        return balance;
    }

    public String getUsername() {
        return username;
    }

    public String getId() {
        return id;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public void setId(String id) {
        if(this.id == null){
            this.id = id;
            this.setBalance(5000D);
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return String.format("UserDao(id = %s, username = %s, password = %s)", id, username, password);
    }

    public String getPassword() {
        return password;
    }

  
    public void setPassword(String password) {
        this.password =  DigestUtils.sha256Hex(password);
    }
}
