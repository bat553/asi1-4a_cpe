$(document).ready(function() {
	verify_user();
	$("#form-login").submit(function (e) {
		e.preventDefault();
		$.ajax("/api/auth/login",
			{
				method: "POST",
				data: JSON.stringify({
					'username': $("#username").val(),
					'password': $("#password").val()
				}),
				contentType: 'application/json',
				dataType: 'json',
				success: function(data, status, xhr) {   // success callback function
					window.location.replace("card.html");
				},
				error: function(jqXhr, textStatus, errorMessage) { // error callback
					if(jqXhr.responseJSON.message === "errors.auth.bad_login") {
						alert("Identifiant incorrect");
					}
				}
			}
		);
	});
});


function verify_user() {
	$.ajax("/api/users/current",
		{
			dataType: 'json',
			success: function(data, status, xhr) {   // success callback function
				window.location.replace("card.html");
			},
			error: function(jqXhr, textStatus, errorMessage) { // error callback

			}
		}
	);
}

function setCookie(cname, cvalue, exdays) {
	const d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	let expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(name) {
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
		if (begin != 0) return null;
	}
	else {
		begin += 2;
		var end = document.cookie.indexOf(";", begin);
		if (end == -1) {
			end = dc.length;
		}
	}
	return decodeURI(dc.substring(begin + prefix.length, end));
} 
