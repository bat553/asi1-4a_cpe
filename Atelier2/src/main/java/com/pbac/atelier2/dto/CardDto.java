package com.pbac.atelier2.dto;

import com.pbac.atelier2.models.User;

public class CardDto {

    private String id;
    private String family_name;
    private String name;
    private String img;
    private String description;
    private int hp;
    private int energy;
    private int attack;
    private int defence;
    private Double price;
    private boolean onSale;
    private User user;

    public CardDto() {
    }

    public CardDto(String id,String family_name,String name,User user, String img,String description,int hp,int energy,int attack,int defence,Double price,boolean onSale) {
        this.id = id;
        this.family_name = family_name;
        this.name = name;
        this.user = user;
        this.img = img;
        this.description = description;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.defence = defence;
        this.price = price;
        this.onSale= onSale;


    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public int getEnergy() {
        return energy;
    }

    public int getHp() {
        return hp;
    }

    public String getFamily_name() {
        return family_name;
    }

    public String getDescription() {
        return description;
    }

    public String getImg() {
        return img;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Double getPrice() {
        return price;
    }

    public boolean isOnSale() {
        return onSale;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setOnSale(boolean onSale) {
        this.onSale = onSale;
    }

    @Override
    public String toString() {
        return String.format("Card(id = %s, name = %s)", id, name);
    }

}
