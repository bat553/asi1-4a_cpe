package com.pbac.atelier2.dto;

public class UsersDto {
	
	private String id;
	private String username;
	private Double balance;
	private String password;
	
	public UsersDto() {
		
	}

	public String getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public Double getBalance() {
		return balance;
	}

	public String getPassword() {
		return password;
	}
	

}
