package com.pbac.atelier2.dto;

import com.pbac.atelier2.models.Card;
import com.pbac.atelier2.models.User;

import java.text.ParseException;

public class TransactionsDto {

    private String transactionDate;
    private User buyer;
    private User seller;
    private Double prix;
    private Card card;

    public TransactionsDto() {

    }

    public TransactionsDto(String transactionDate, User buyer, User seller, Double prix, Card card) throws ParseException {
        super();
        this.transactionDate = transactionDate;
        this.buyer = buyer;
        this.seller = seller;
        this.prix = prix;
        this.card = card;
    }

    public void setTransactionDate(String d) {
        transactionDate = d;
    }
    public String getTransactionDate() {
        return this.transactionDate;
    }

    public Double getPrix() {
        return prix;
    }

    public Card getcard() {
        return card;
    }

    public User getbuyer() {
        return buyer;
    }

    public User getseller() {
        return seller;
    }

}
