package com.pbac.atelier2.controllers;

import com.pbac.atelier2.dto.CardDto;
import com.pbac.atelier2.exceptions.TransactionError;
import com.pbac.atelier2.exceptions.UserError;
import com.pbac.atelier2.forms.BuyCardForm;
import com.pbac.atelier2.forms.ChangeCardForm;
import com.pbac.atelier2.mappers.CardMapper;
import com.pbac.atelier2.models.Card;
import com.pbac.atelier2.models.Transactions;
import com.pbac.atelier2.models.User;
import com.pbac.atelier2.services.AuthentificationService;
import com.pbac.atelier2.services.CardsService;
import com.pbac.atelier2.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class CardsController {
	@Autowired
	CardsService cService;

	@Autowired
	AuthentificationService aService;

	@Autowired
	UsersService uService;
	
	@RequestMapping(value="/api/cards", method = RequestMethod.POST)
	public Card addCard(@RequestBody CardDto cardDto) {
		Card card = CardMapper.INSTANCE.toModel(cardDto);
		cService.addCard(card);
		return card;
	}
	
	@RequestMapping(value="/api/cards/{id}", method=RequestMethod.GET)
	public Card getCard(@PathVariable String id) {
		return cService.getCard(id);
	}

	@RequestMapping(method= RequestMethod.GET,value="/api/cards")
	public Iterable<Card> getCards() {
		return cService.getCards();
	}

	@RequestMapping(method= RequestMethod.GET,value="/api/cardsUser/{id}")
	public Iterable<Card> getCardsUser(@PathVariable String id) {
		return cService.getCardsUser(id);
	}

	@RequestMapping(method= RequestMethod.GET,value="/api/cardsOnSale")
	public Iterable<Card> getCardsOnSale() {
		return cService.getCardsOnSale();
	}

	@RequestMapping(method= RequestMethod.POST,value="/api/cards/buy")
	public Transactions buyCard(@RequestBody BuyCardForm cardForm, @CookieValue(value = "JWT") String jwt, HttpServletResponse response) {
		String username = aService.getUsername(jwt);
		User user = uService.findByUsername(username);

		if (user == null){
			throw new UserError("unknown");
		}

		Transactions transaction = cService.buyCard(cardForm.getCardId(), user);
		if (transaction == null){
			throw new TransactionError("error");
		}
		return transaction;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/api/cards")
	public Card setCardSellStatus(@CookieValue(value = "JWT") String jwt, @RequestBody ChangeCardForm changeCardForm){
		String username = aService.getUsername(jwt);
		User user = uService.findByUsername(username);

		if (user == null){
			throw new UserError("unknown");
		}

		return cService.setCardSellStatus(changeCardForm, user);
	}
}
