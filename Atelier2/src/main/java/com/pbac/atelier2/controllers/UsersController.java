package com.pbac.atelier2.controllers;

import com.pbac.atelier2.exceptions.AuthError;
import com.pbac.atelier2.exceptions.UserError;
import com.pbac.atelier2.models.User;
import com.pbac.atelier2.services.AuthentificationService;
import com.pbac.atelier2.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {

    @Autowired
    UsersService usersService;

    @Autowired
    AuthentificationService authentificationService;

    @RequestMapping(method = RequestMethod.GET, value = "/api/users/{id}")
    public User getUser(@PathVariable String id){
        return usersService.getUser(id);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/api/users")
    public User createUser(@RequestBody User userDao){
        User user = usersService.addUser(userDao);
        if (user == null){
            throw new UserError("creation");
        }
        return user;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/users")
    public Iterable<User> getUsers(){
        return usersService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/users/current")
    public User getCurrentUser(@CookieValue(value = "JWT") String jwt){

        String username = authentificationService.getUsername(jwt);

        User user = usersService.findByUsername(username);
        if (user != null){
            return user;
        } else {
            throw new AuthError("bad_token");
        }
    }
}
