package com.pbac.atelier2.controllers;

import com.pbac.atelier2.dto.TransactionsDto;
import com.pbac.atelier2.mappers.TransactionMapper;
import com.pbac.atelier2.models.Transactions;
import com.pbac.atelier2.services.TransactionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransactionsController {

    @Autowired
    TransactionsService tService;

    @RequestMapping(method= RequestMethod.POST,value="/api/transactions")
    public void addTransactions(@RequestBody TransactionsDto transactions) {
        Transactions tmpTransaction = TransactionMapper.INSTANCE.toModel(transactions);
        tService.addTransaction(tmpTransaction);
    }

    @RequestMapping(method= RequestMethod.GET,value="/api/transactions")
    public Iterable<Transactions> getTransactions() {
        return tService.getTransactions();
    }

    @RequestMapping(method=RequestMethod.GET,value="/api/transactions/{id}")
    public Transactions getTransaction(@PathVariable String id) {
        return tService.getTransaction(Integer.valueOf(id));
    }

}
