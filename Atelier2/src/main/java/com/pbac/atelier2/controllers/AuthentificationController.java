package com.pbac.atelier2.controllers;

import com.pbac.atelier2.exceptions.AuthError;
import com.pbac.atelier2.forms.LoginForm;
import com.pbac.atelier2.models.User;
import com.pbac.atelier2.services.UsersService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@RestController
public class AuthentificationController {

    private final String JWT_SECRET = "Th0m4sceGrosBG";

    @Autowired
    UsersService usersService;


    @RequestMapping(method = RequestMethod.POST, value = "/api/auth/login")
    public User login(@RequestBody LoginForm loginForm, HttpServletResponse response) {
        User user = usersService.checkLogin(loginForm);
        if (user == null){
            throw new AuthError("bad_login");
        }
        String jwt = Jwts.builder()
                .setIssuer("Aterlier2")
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(6, ChronoUnit.HOURS)))
                .signWith(SignatureAlgorithm.HS256, JWT_SECRET.getBytes(StandardCharsets.UTF_8))
                .setSubject(user.getUsername()).compact();

        Cookie cookie = new Cookie("JWT", jwt);
        cookie.setPath("/");
        response.addCookie(cookie);
        return user;
    }

}
