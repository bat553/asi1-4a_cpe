package com.pbac.atelier2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class Atelier2Application {

	public static void main(String[] args) {
		SpringApplication.run(Atelier2Application.class, args);
	}

}
