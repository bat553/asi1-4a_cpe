package com.pbac.atelier2.services;

import com.pbac.atelier2.exceptions.CardError;
import com.pbac.atelier2.exceptions.TransactionError;
import com.pbac.atelier2.exceptions.UserError;
import com.pbac.atelier2.forms.ChangeCardForm;
import com.pbac.atelier2.models.Card;
import com.pbac.atelier2.models.Transactions;
import com.pbac.atelier2.models.User;
import com.pbac.atelier2.repositories.CardsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CardsService {
	@Autowired
	CardsRepository cRepository;

	@Autowired
	TransactionsService tService;

	@Autowired
	UsersService uService;

	public void addCard(Card c) {
		c.setId(UUID.randomUUID().toString());
		Card createdCard=cRepository.save(c);
		System.out.println(createdCard);
	}

	public Transactions buyCard(String cardId, User buyer) {
		Card card;
		Optional<Card> tmpCard = cRepository.findById(cardId);
		if (tmpCard.isPresent()) {
			card = tmpCard.get();

			if(buyer != null){
				if(!card.isOnSale()){
					throw new CardError("no_on_sale");
				}

				if(buyer.getBalance() == null || buyer.getBalance() < card.getPrice()){
					throw new TransactionError("no_funds");
				}

				User seller = uService.getUser(card.getUser().getId());
				if (seller == null){
					throw new UserError("unknown");
				}

				//création d'une transaction
				Transactions transaction = tService.addTransaction(new Transactions(buyer, seller, card));

				//Update de la card
				card.setOnSale(false);
				card.setUser(buyer);

				// Update users
				uService.updateBalance(seller, seller.getBalance() + card.getPrice());
				uService.updateBalance(buyer, buyer.getBalance() - card.getPrice());

				cRepository.save(card);
				return transaction;
			}
		}
		return null;
	}
	
	public Card getCard(String id) {
		Optional<Card> cOpt = cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}

	public Iterable<Card> getCards() {
		return cRepository.findAll();
	}

	public List<Card> getCardsUser(String id) {
		Iterable<Card> tmpCards = cRepository.findAll();
		List<Card> cards = new ArrayList<>();
		System.out.println(id);

		for (Card card:tmpCards) {
			System.out.println(card.getUser().getId());
			System.out.println(card.getUser().getId().compareTo(id));
			if(card.getUser().getId().compareTo(id) == 0){
				cards.add(card);
			}
		}
		return cards;
	}

	public List<Card> getCardsOnSale() {
		Iterable<Card> tmpCards = cRepository.findAll();
		List<Card> cards = new ArrayList<>();
		for (Card card:tmpCards) {
			if(card.isOnSale()){
				cards.add(card);
			}
		}
		return cards;
	}

	public Card setCardSellStatus(ChangeCardForm cardForm, User user) {
		Card card = this.getCard(cardForm.getId());
		if (card == null){
			throw new CardError("not_found");
		}
		if (cardForm.getPrice() < 1){
			throw new CardError("null_price");
		}

		if(!card.getUser().getId().equals(user.getId())){
			throw new CardError("not_owned_by_you");
		}

		card.setOnSale(cardForm.isOnSale());
		card.setPrice(cardForm.getPrice());

		this.cRepository.save(card);

		return card;
	}
}
