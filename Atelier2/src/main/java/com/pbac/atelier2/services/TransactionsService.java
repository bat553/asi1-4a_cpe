package com.pbac.atelier2.services;

import com.pbac.atelier2.models.Transactions;
import com.pbac.atelier2.repositories.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class TransactionsService {

    @Autowired
    TransactionsRepository tRepository;

    public Transactions addTransaction(Transactions transaction) {
        transaction.setId(UUID.randomUUID().toString());
        return tRepository.save(transaction);
    }

    public Transactions getTransaction(int id) {
        Optional<Transactions> hOpt =tRepository.findById(id);
        if (hOpt.isPresent()) {
            return hOpt.get();
        }else {
            return null;
        }
    }

    public Iterable<Transactions> getTransactions() {
        return tRepository.findAll();

    }
}
