package com.pbac.atelier2.services;

import com.pbac.atelier2.forms.LoginForm;
import com.pbac.atelier2.models.User;
import com.pbac.atelier2.repositories.UsersRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UsersService {

    @Autowired
    UsersRepository userRepository;

    public User addUser(User userDao){
        userDao.setId(UUID.randomUUID().toString());
        return userRepository.save(userDao);
    }

    public User getUser(String id){
        System.out.println("On essaye de chercher l'user avec l'identifiant = " + id);
        Optional<User> usersDaoOptional = userRepository.findById(id);
        if(usersDaoOptional.isPresent()){
            return usersDaoOptional.get();
        } else {
            return null;
        }
    }

    public User checkLogin(LoginForm loginForm){
        Optional<User> userOptional = userRepository.findByUsername(loginForm.getUsername());
        if(userOptional.isPresent() && userOptional.get().getPassword() != null){
            String hashed_password_user = DigestUtils.sha256Hex(loginForm.getPassword());
            if(hashed_password_user.equals(userOptional.get().getPassword())){
                return userOptional.get();
            }
        }
        return null;
    }

    public User findByUsername(String username){
        Optional<User> userOptional = userRepository.findByUsername(username);
        if(userOptional.isPresent()){
            return userOptional.get();
        }
        return null;
    }

    public Iterable<User> findAll(){
        return userRepository.findAll();
    }

    public User updateBalance(User user, Double newBalance){
        user.setBalance(newBalance);
        userRepository.save(user);
        return user;
    }

}
