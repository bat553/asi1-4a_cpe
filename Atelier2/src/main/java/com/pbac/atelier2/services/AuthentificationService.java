package com.pbac.atelier2.services;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.DefaultJwtSignatureValidator;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;

@Service
public class AuthentificationService {

    SignatureAlgorithm sa = SignatureAlgorithm.HS256;
    SecretKeySpec secretKeySpec = new SecretKeySpec(getSecret(), sa.getJcaName());

    private final DefaultJwtSignatureValidator validator = new DefaultJwtSignatureValidator(sa, secretKeySpec);

    public byte[] getSecret(){
        String JWT_SECRET = "Th0m4sceGrosBG";
        return JWT_SECRET.getBytes();
    }

    public boolean checkJwt(String jwt){
        String[] chunk = jwt.split("\\.");
        String tokenWithoutSignature = chunk[0] + "." + chunk[1];
        String signature = chunk[2];

        return validator.isValid(tokenWithoutSignature, signature);
    }

    public String getUsername(String jwt){
        return (String) Jwts.parser()
            .setSigningKey(getSecret())
            .parseClaimsJws(jwt)
            .getBody()
            .getSubject();
    }

}
