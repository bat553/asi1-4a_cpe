package com.pbac.atelier2.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.pbac.atelier2.models.Card;

public interface CardsRepository extends CrudRepository<Card, String>{
	
	public List<Card> findByName(String name); 
}
