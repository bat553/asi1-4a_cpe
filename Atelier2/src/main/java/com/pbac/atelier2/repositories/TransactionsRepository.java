package com.pbac.atelier2.repositories;

import com.pbac.atelier2.models.Transactions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionsRepository extends CrudRepository<Transactions, Integer> {
        //public List<TransactionsDao> findByName(String name);

}
