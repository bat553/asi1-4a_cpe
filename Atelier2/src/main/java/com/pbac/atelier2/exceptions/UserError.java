package com.pbac.atelier2.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class UserError extends RuntimeException {
    public UserError(String code){
        super(String.format("errors.user.%s", code));
    }

}

