package com.pbac.atelier2.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.pbac.atelier2.dto.UsersDto;
import com.pbac.atelier2.models.User;

@Mapper
public interface UserMapper {
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	User toModel(UsersDto userDto);
}
