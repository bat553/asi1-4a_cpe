package com.pbac.atelier2.mappers;

import com.pbac.atelier2.dto.CardDto;
import com.pbac.atelier2.dto.TransactionsDto;
import com.pbac.atelier2.models.Card;
import com.pbac.atelier2.models.Transactions;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CardMapper {
    CardMapper INSTANCE = Mappers.getMapper(CardMapper.class);
    Card toModel(CardDto cardDto);
}
