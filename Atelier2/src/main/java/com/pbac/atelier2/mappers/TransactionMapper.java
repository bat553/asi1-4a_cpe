package com.pbac.atelier2.mappers;

import com.pbac.atelier2.dto.TransactionsDto;
import com.pbac.atelier2.models.Transactions;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import javax.transaction.Transaction;

@Mapper
public interface TransactionMapper {
    TransactionMapper INSTANCE = Mappers.getMapper(TransactionMapper.class);
    @Mapping(target="transactionDate", dateFormat="dd/MM/yyyy")
    Transactions toModel(TransactionsDto transactionDto);
}
