package com.pbac.atelier2.models;


import com.pbac.atelier2.exceptions.TransactionError;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="transaction")
public class Transactions {
    @Id
    private String id;
    private Date transactionDate;

    @OneToOne
    @JoinColumn(name = "buyer_fk")
    private User buyer;

    @OneToOne
    @JoinColumn(name = "seller_fk")
    private User seller;

    private Double prix;

    @OneToOne
    @JoinColumn(name = "card_fk")
    private Card card;

    public Transactions() {}

    public Transactions(User buyer, User seller, Card card) {
        super();
        this.id = UUID.randomUUID().toString();
        this.transactionDate = new Date();
        if(buyer.getId().equals(seller.getId())){
            throw new TransactionError("same_seller_buyer");
        }

        this.buyer = buyer;
        this.seller = seller;
        this.card = card;
        this.prix = card.getPrice();


    }

    public Date getTransactionDate() {
        return this.transactionDate;
    }

    public Double getPrix() {
        return prix;
    }

    public String getId() {
        return id;
    }

    public Card getcard() {
        return card;
    }

    public User getbuyer() {
        return buyer;
    }

    public User getseller() {
        return seller;
    }

    public void setbuyer(User buyer) {
        this.buyer = buyer;
    }

    public void setcard(Card card) {
        this.card = card;
    }

    public void setseller(User seller) {
        this.seller = seller;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
}

